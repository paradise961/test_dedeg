import React, { useState } from 'react'
import { Field } from 'formik'

const MyInput  = (props) => {
    console.log(props)
    return (
        <input type="text" value={props.value} onChange={(e) => props.onChange(e)} />
    )
}

function RegisterPage(props) {
    const [value, setValue] = useState("test")

    const _onChange = (e) => {
        console.log(e.target.value)
        setValue(e.target.value)
    }

    // const MyInput = (props) => {
    //     return (
    //         <input value={value} onChange={_onChange} type="text"/>
    //     )
    // }

    return (
        <MyInput value={value} onChange={_onChange}></MyInput>
                            
    )
}

export default RegisterPage